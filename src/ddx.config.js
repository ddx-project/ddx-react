const config = {
  title: "DD-People",
  subtitle:
    "The distributed database of people (DD-People) is a ownerless and trustless registry of individuals. It is backed the open index protocol to place data into the immutable transactions of the FLO blockchain.",
  ipfs: {
    apiUrl: "https://ipfs.io/ipfs/"
  },
  frontPage: {
    numberOfNewestRecordsToShow: 9,
    aboutTitle: "DD-People is more than just a registry",
    aboutShortText:
      "We believe that no one but you should own your data. We created the ecosystem of distributed databases (DDX).",
    aboutImage: "one.png"
  },
  oip: {
    perPage: 12,
    daemonApiUrl: "https://api.oip.io/oip/o5/",
    baseTemplate: "tmpl_B6E9AF9B",
    requiredTemplates: [],
    addressesWhiteList: [] //"F6R95XtThjfDr2uGgPPAyG3QS2749osmdC"]
  },
  cardInfo: {
    name: {
      tmpl: "tmpl_20AD45E7",
      name: "name"
    },
    surname: {
      tmpl: "tmpl_B6E9AF9B",
      name: "surname"
    },
    placeOfBirth: {
      tmpl: "tmpl_B6E9AF9B",
      name: "placeOfBirth"
    },
    description: {
      tmpl: "tmpl_20AD45E7",
      name: "description"
    },
    avatarRecord: {
      tmpl: "tmpl_20AD45E7",
      name: "avatar"
    }
  },
  imageHandler: {
    thumbnail: {
      tmpl: "tmpl_1AC73C98",
      name: "thumbnailAddress"
    },
    main: {
      tmpl: "tmpl_1AC73C98",
      name: "imageAddress"
    }
  }
};

export { config };
